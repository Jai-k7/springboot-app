FROM openjdk:8
MAINTAINER "JAI KISHORE<jai9376@gmail.com>"
COPY  target/demo1-0.0.1-SNAPSHOT.jar ./ROOT.jar
WORKDIR .
EXPOSE 8080
ENTRYPOINT ["java","-jar","ROOT.jar"]
